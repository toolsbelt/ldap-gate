class User:

    def __init__(self, document_id, login, password, group_id):
        self.document_id = document_id
        self.login = login
        self.password = password
        self.group_id = group_id

    def json(self):
        return {
            "document_id": self.document_id,
            "login": self.login,
            "password": self.password,
            "group_id": self.group_id
        }
