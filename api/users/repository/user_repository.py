from bson.json_util import dumps, loads

from config.mongo import database

COLLECTIONS = database["users"]


def save(user):
    COLLECTIONS.save(user)


def find_all():
    result = COLLECTIONS.find({})
    return [{
        "id": str(user["_id"]),
        "login": user["login"],
        "group_id": user["group_id"],
    } for user in result]


def find_by_id(login):
    user = loads(dumps(COLLECTIONS.find_one({"login": login})))
    return {
        "id": str(user["_id"]),
        "login": user["login"],
        "group_id": user["group_id"]
    }
