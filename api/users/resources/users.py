from flask_bcrypt import Bcrypt
from flask_restful import Resource, reqparse, request

from api.users.repository.user_repository import save, find_all


class Users(Resource):

    def post(self):
        bcrypt = Bcrypt(None)
        pw_hash = bcrypt.generate_password_hash(request.json['password'])
        request.json['password'] = pw_hash

        parse = reqparse.RequestParser()
        parse.add_argument("document_id", required=True, help="Document id is required!")
        parse.add_argument("login", required=True, help="Login is required!")
        parse.add_argument("password", required=False)
        parse.add_argument("group_id", required=True, help="Group id is required!")
        data = parse.parse_args()

        save(data)
        return {"message": "User create"}, 201

    def get(self):
        return {"users": find_all()}, 200
