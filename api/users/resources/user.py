from flask_restful import Resource
from api.users.repository.user_repository import find_by_id


class User(Resource):
    def put(self, _login):
        pass

    def delete(self, _login):
        pass

    def get(self, _login):
        return  {"user": find_by_id(_login)}, 200

    def patch(self, _login):
        pass
