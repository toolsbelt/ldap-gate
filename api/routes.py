from flask_restful import Api

from api.users.resources.user import User
from api.users.resources.users import Users

api = Api()

api.add_resource(Users, '/users')
api.add_resource(User, '/user/<string:_login>')
