from api.routes import api
from flask import Flask

app = Flask(__name__)
api.init_app(app)

if __name__ == '__main__':
    app.run(debug=True)
